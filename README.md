# website

## Installieren

* go get -u github.com/spf13/cobra
* go install github.com/spf13/cobra/cobra

```
cobra init --author "Thorsten Klöhn thorstenkloehn@gmail.com" --license apache --viper 
```

* [Viper](https://github.com/spf13/viper)
* [Cobra Generator](https://github.com/spf13/cobra/blob/master/cobra/README.md)
* [Cobra](https://github.com/spf13/cobra)

## Test Seite
