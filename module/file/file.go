package file

import (
	"fmt"
	"github.com/spf13/viper"
	"io/ioutil"
	"os"
	"os/exec"
)

type Installieren struct {
	Website_Titel string
}

func (start *Installieren) Start() {
	os.Mkdir(start.Website_Titel+"/controller", 0777)
	os.Mkdir(start.Website_Titel+"/model", 0777)
	os.Mkdir(start.Website_Titel+"/view", 0777)
	fmt.Println("File Test Seite")
	viper.SetDefault("Mysql_Hosting", "Hallo")
	viper.WriteConfig()
	cmd := exec.Command("go", "mod", "init", start.Website_Titel)
	cmd.Dir = start.Website_Titel
	cmd.Run()

	var mainInhalt = `package main

import (
	"fmt"
	"net/http"
    "` + start.Website_Titel + `/controller"
)

func main() {
	router := http.NewServeMux()
	router.HandleFunc("/",controller.Default)
	fmt.Println("http://localhost:8080")
	http.ListenAndServe(":8080",router)
}`

	ioutil.WriteFile(start.Website_Titel+"/main.go", []byte(mainInhalt), 0777)

	var gitnore = `
/.idea/
/config/
`
	ioutil.WriteFile(start.Website_Titel+"/.gitignore", []byte(gitnore), 0777)
	cmd1 := exec.Command("git", "init")
	cmd1.Dir = start.Website_Titel
	cmd1.Run()

	var controllervar = `package controller

import "net/http"

func Default(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Test Seite"))
}`
	ioutil.WriteFile(start.Website_Titel+"/controller/default.go", []byte(controllervar), 0777)

}
