/*
Copyright © 2022 Thorsten Klöhn thorstenkloehn@gmail.com

*/
package cmd

import (
	"fmt"
	"github.com/spf13/viper"
	"github.com/thorstenkloehn/website/module/file"
	"os"

	"github.com/spf13/cobra"
)

// newCmd represents the new command
var newCmd = &cobra.Command{
	Use:   "new",
	Short: "Neu Website erstellen",
	Long:  `Neu Website erstellen`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) < 1 {
			fmt.Println("Kein Projekt Name")
			os.Exit(10)
		} else {
			if cfgFile != "" {
				// Use config file from the flag.
				viper.SetConfigFile(cfgFile)
			} else {
				os.Mkdir(args[0], 0777)
				os.Mkdir(args[0]+"/config", 0777)
				os.Create(args[0] + "/config/config.yaml")
				viper.AddConfigPath(args[0] + "/config")
				viper.SetConfigType("yaml")
				viper.SetConfigName("config")
			}

			viper.AutomaticEnv() // read in environment variables that match

			viper.SetDefault("Website_Titel", args[0])
			viper.WriteConfig()
			fmt.Println("Erstellt", viper.Get("Website_Titel"))
			if err := viper.ReadInConfig(); err == nil {
				fmt.Fprintln(os.Stderr, "Erstellt neue Verzeichniss:", viper.ConfigFileUsed())
				var installieren = file.Installieren{}
				installieren.Website_Titel = viper.GetString("Website_Titel")
				installieren.Start()
			} else if cfgFile != "" {
				// Use config file from the flag.
				viper.SetConfigFile(cfgFile)
			}
		}

	},
}

func init() {
	rootCmd.AddCommand(newCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	newCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	newCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
